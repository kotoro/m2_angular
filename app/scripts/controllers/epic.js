'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webappApp
 */
angular.module('webappApp').controller('EpicCtrl',
	function ($scope, $location, restService, saverService) {

	this.awesomeThings = [
						  'HTML5 Boilerplate',
						  'AngularJS',
						  'Karma'
						  ];

	$scope.addStory = function() {
		$scope.user_story.epic = {"id": $scope.epic.id};
		restService.post("/user_story/", $scope.user_story).then(function () {
			updateEpic();
		});
	};

	$scope.removeStory = function(id){
		alert("remove with :" + id);
		restService.remove("/user_story/",id).then(function () {
			updateEpic();
		});
	};

	$scope.seeDetails = function(object){
		saverService.set("story",object);
		$location.path('/user_story/manager');
	};

	$scope.save = function() {
		var tmp = $scope.epic;
		delete tmp.userstory;
		restService.put("/epic/", tmp).then(function (response){
			$scope.epic = response;
		});
	};

	var updateEpic = function() {
		restService.get("/epic/", saverService.get("epic").id).then(function (response){
			$scope.epic = response;
		});
	}

	if (!saverService.get("epic")) {
		$location.path("/");
	} else {
		updateEpic();
	}
});