'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:TaskCtrl
 * @description
 * # TaskCtrl
 * Controller of the webappApp
 */
angular.module('webappApp').controller('TaskCtrl',
	function ($scope, $location, restService, saverService) {

	this.awesomeThings = [
						  'HTML5 Boilerplate',
						  'AngularJS',
						  'Karma'
						  ];

	$scope.listDevelopers = [];
	$scope.task = saverService.get("task");

	$scope.save = function() {
		var tmp = $scope.task;
		tmp.userStory = {"id" : $scope.task.userStory};
		tmp.developer = {"id" : $scope.task.developer};

		restService.put("/task/", tmp).then(function (response){
			$scope.task = response;
		});
	};

	$scope.previous = function() {
		$location.path("/user_story/manager");
	};

	var getDevelopers = function() {
		restService.getAll("/developer/").then(function (response){
			$scope.listDevelopers = response;
		});
	};
	console.log($scope.task);
	getDevelopers();
});