'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webappApp
 */
angular.module('webappApp').controller('MainCtrl',
	function ($scope, $location, restService, saverService) {

	this.awesomeThings = [
						  'HTML5 Boilerplate',
						  'AngularJS',
						  'Karma'
						  ];

	$scope.listEpics = [];

	$scope.addEpic = function() {
		restService.post("/epic/", $scope.epic).then(function() {
			showEpics();
		});
	};

	$scope.removeEpic = function(id){
		alert("remove with :" + id);
		restService.remove("/epic/",id).then(function() {
        	showEpics();
		});
	};

	$scope.seeDetails = function(object){
		saverService.set("epic",object);
		$location.path('/epic/manager');
	};

	var showEpics = function(){
		restService.getAll("/epic/").then(function (response){
			$scope.listEpics = response;
		});
	};
	
	showEpics();
});