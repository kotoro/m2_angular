'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:StoryCtrl
 * @description
 * # MainCtrl
 * Controller of the webappApp
 */
angular.module('webappApp').controller('StoryCtrl',
	function ($scope, $location, restService, saverService) {

	this.awesomeThings = [
						  'HTML5 Boilerplate',
						  'AngularJS',
						  'Karma'
						  ];

	$scope.listDevelopers = [];
	$scope.user_story = saverService.get("story");
	$scope.addTask = function(){
		$scope.task.userStory = {"id": $scope.user_story.id};
		$scope.task.status = "NOT_STARTED";
		restService.post("/task/", $scope.task).then(function () {
			updateUserStory();
		});
	};

	$scope.removeTask = function(id){
		alert("remove with :" + id);
		restService.remove("/task/",id).then(function () {
        	updateUserStory();
		});
	};

	$scope.seeDetails = function(object){
		saverService.set("task",object);
		$location.path('/task/manager');
	};

	$scope.save = function() {
		var tmp = $scope.user_story;
		delete tmp.tasks;
		tmp.epic = {"id" : $scope.user_story.epic};
		restService.put("/user_story/", tmp).then(function (response){
			$scope.user_story = response;
		});
	};

	var updateUserStory = function() {
		restService.get("/user_story/", $scope.user_story.id).then(function (response){
			$scope.user_story = response;
		});
	};

	var getDevelopers = function() {
		restService.getAll("/developer/").then(function (response){
			$scope.listDevelopers = response;
		});
	};

	if (!saverService.get("story")) {
    		$location.path("/");
	} else {
		updateUserStory();
	}

	getDevelopers();
});