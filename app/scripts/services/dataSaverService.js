angular.module('webappApp').factory('saverService', function() {
	var savedData = new Map();

	function set(key,data) {
		savedData.set(key,data);
	}
	function get(key) {
		return savedData.get(key);
	}

	return {
		set: set,
		get: get
	};
});