/**
 * @ngdoc function
 * @name webappApp.serice:restService
 * @description
 * # restService
 * Service of the webappApp
 */
angular.module('webappApp').service('restService', ["$http", "$q", function ($http,$q) {
	this.getAll = function(url) {
		var deferred = $q.defer();
		$http.get(url).success(function (response) {
			deferred.resolve(response);
		}).error(function (response) {
			alert("Error : Can not retrieve data.");
			deferred.reject(response);
		});

		return deferred.promise;
	};

	this.get = function(url,id) {
		var deferred = $q.defer();
		$http.get(url + '/' + id).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status, headers, config) {
			alert("Error : Can not retrieve data.");
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.post = function(url,object) {
		var deferred = $q.defer();

		$http.post(url,object).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status, headers, config) {
			alert("Error : Can not post data.");
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.put = function(url,object) {
		var deferred = $q.defer();

		$http.put(url,object).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status, headers, config) {
			alert("Error : Can not put data.");
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.remove = function(url,id) {
		var deferred = $q.defer();

		$http.delete(url + '/' + id).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error( function (data, status, headers, config) {
			alert("Error : Can not delete data.");
			deferred.reject(data);
		});

		return deferred.promise;
	}
}]);