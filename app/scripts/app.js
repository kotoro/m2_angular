'use strict';

/**
 * @ngdoc overview
 * @name webappApp
 * @description
 * # webappApp
 *
 * Main module of the application.
 */
angular
  .module('webappApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/epic/manager', {
              templateUrl: 'views/epic.html',
              controller: 'EpicCtrl',
              controllerAs: 'epic'
            })
      .when('/user_story/manager', {
        templateUrl: 'views/user_story.html',
        controller: 'StoryCtrl',
        controllerAs: 'story'
      })
      .when('/task/manager', {
        templateUrl: 'views/task.html',
        controller: 'TaskCtrl',
        controllerAs: 'task'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
